package com.sk.ks.tikameshi.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.sk.ks.tikameshi.R;
import com.sk.ks.tikameshi.adapter.RowAdapter;
import com.sk.ks.tikameshi.adapter.RowItem;
import com.sk.ks.tikameshi.gnavi.Gnavi;
import com.sk.ks.tikameshi.gps.Gps;

import java.util.ArrayList;

/**
 * Activity
 * 検索結果画面
 */
public class ResultsActivity extends AppCompatActivity {
    Context context;
    // GPS用
    private LocationManager mLocationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_result);
        //actionbar
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        Gps gpss = new Gps();
        String[] gps_arr = gpss.getGps();
        Intent i = getIntent();
        Bundle extras = i.getExtras();
        String keyword = extras.getString("keyword");
        String range = extras.getString("range");
        Gnavi gnavi = new Gnavi(String.valueOf(gps_arr[0]),String.valueOf(gps_arr[1]),keyword,range);
        gnavi.excute(new Gnavi.exeCallback() {
            @Override
            public void onSuccess(ArrayList<RowItem> rowItems) {
                final ListView lv = (ListView) findViewById(R.id.listview);
                lv.setAdapter(new RowAdapter(context, R.layout.result_row, rowItems));
            }

            @Override
            public void onError() {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle(R.string.result_error);
                alertDialogBuilder.setMessage(R.string.result_back);
                alertDialogBuilder.setPositiveButton(R.string.result_ok,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}