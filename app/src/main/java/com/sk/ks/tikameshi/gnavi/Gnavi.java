package com.sk.ks.tikameshi.gnavi;

import android.location.Location;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.sk.ks.tikameshi.adapter.RowItem;
import com.sk.ks.tikameshi.volley.VolleyHelper;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ぐるなびAPI
 * リクエスト/レスポンス
 */
public class Gnavi {
    private final String AccessKey = "e34a3c3e3184c558eefdbff90de2087f";
    private final String g_schema = "http";
    private final String g_host = "api.gnavi.co.jp";
    private final String g_path = "/RestSearchAPI/20150630/";
    private String latitude;
    private String longitude;
    private String keyword;
    private String range;


    private ArrayList<RowItem> mItems = new ArrayList<RowItem>();

    public Gnavi(String latitude,String longitude,String keyword,String range){
        this.latitude = latitude;
        this.longitude = longitude;
        this.keyword = keyword;
        this.range = range;
    }

    public interface exeCallback {
        public void onSuccess(ArrayList<RowItem> rowItems);
        public void onError();
    }


    private void createRequest(){

    }

    public void excute(final exeCallback instance){
        VolleyHelper volleyHelper = VolleyHelper.getInstance();
        Map<String, String> params = new HashMap<String, String>();
        // params
        params.clear();
        params.put("keyid", AccessKey);
        params.put("format", "json");
        params.put("freeword", keyword);
        params.put("latitude", latitude);
        params.put("longitude", longitude);
        params.put("range", range);


        volleyHelper.exeQueueGET(g_schema, g_host, g_path, params,
                new VolleyHelper.exeQueueCallback() {

                    @Override
                    public void onSuccessCallback(JSONObject response) {
                        try {
                            //response パース
                            JSONArray restArray = response.getJSONArray("rest");
                            for (int i = 0; i < restArray.length(); i++) {
                                final RowItem rowItem = new RowItem();
                                JSONObject rest_obj = restArray.getJSONObject(i);
                                rowItem.setName(checkExistKey(rest_obj, "name"));
                                rowItem.setLatitude(checkExistKey(rest_obj, "latitude"));
                                rowItem.setLongitude(checkExistKey(rest_obj, "longitude"));
                                rowItem.setAddress(checkExistKey(rest_obj, "address"));
                                rowItem.setTel(checkExistKey(rest_obj, "tel"));
                                rowItem.setUrl(checkExistKey(rest_obj, "url"));
                                rowItem.setOpentime(checkExistKey(rest_obj, "opentime"));

                                JSONObject access_obj = rest_obj.getJSONObject("access");
                                rowItem.setA_line(checkExistKey(access_obj, "line"));
                                rowItem.setA_walk(checkExistKey(access_obj, "walk"));
                                rowItem.setA_station(checkExistKey(access_obj, "station"));
                                rowItem.setA_note(checkExistKey(access_obj, "note"));
                                rowItem.setA_station_exit(checkExistKey(access_obj, "station_exit"));

                                JSONObject image_obj = rest_obj.getJSONObject("image_url");
                                rowItem.setShop_image1(checkExistKey(image_obj, "shop_image1"));
                                rowItem.setShop_image2(checkExistKey(image_obj, "shop_image2"));
                                mItems.add(rowItem);
                            }
                            instance.onSuccess(mItems);

                        } catch (JSONException e) {
                            instance.onError();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorCallback(VolleyError error) {

                    }
                }
        );

    }

    private String checkExistKey(JSONObject obj,String key){
        String value = "";
        if (!obj.isNull(key)){
            try {
                value = obj.getString(key);
            } catch (JSONException e) {
                return value;
            }
        }

        return value;
    }
}
