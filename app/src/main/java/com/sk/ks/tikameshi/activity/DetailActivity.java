package com.sk.ks.tikameshi.activity;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.sk.ks.tikameshi.R;
import com.sk.ks.tikameshi.volley.CustomNetWorkImageView;
import com.sk.ks.tikameshi.volley.VolleyHelper;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Activity 
 * 店舗詳細画面
 */
public class DetailActivity extends AppCompatActivity {
    @Bind(R.id.det_shop_number)
    TextView tel_Text;
    private String mUrl;
    private String mTel;
    private String mName;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_detail);

        ButterKnife.bind(this);

        Intent i = getIntent();
        Bundle extras = i.getExtras();

        mUrl = extras.getString("shop_url");

        TextView nameTextView = (TextView) findViewById(R.id.det_shop_name);
        mName = extras.getString("shop_name");
        nameTextView.setText(mName);

        TextView addressTextView = (TextView) findViewById(R.id.det_shop_address);
        addressTextView.setText(extras.getString("shop_address"));

        TextView numTextView = (TextView) findViewById(R.id.det_shop_number);
        mTel = extras.getString("shop_number");
        numTextView.setText(mTel);

        TextView openTextview = (TextView) findViewById(R.id.det_shop_open);
        openTextview.setText(extras.getString("shop_opentime"));

        CustomNetWorkImageView shopImage = (CustomNetWorkImageView) findViewById(R.id.det_shop_image);
        shopImage.setImageUrl(extras.getString("shop_image"), VolleyHelper.getInstance().getImageLoader());
        shopImage.setErrorImageResId(R.mipmap.no_image);

        final WebView webView = (WebView)findViewById(R.id.det_webview);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(mUrl);
        //webvieのタッチイベント取得
        webView.setOnTouchListener(new View.OnTouchListener() {

            public final static int FINGER_RELEASED = 0;
            public final static int FINGER_TOUCHED = 1;
            public final static int FINGER_DRAGGING = 2;
            public final static int FINGER_UNDEFINED = 3;

            private int fingerState = FINGER_RELEASED;


            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (fingerState == FINGER_RELEASED) fingerState = FINGER_TOUCHED;
                        else fingerState = FINGER_UNDEFINED;
                        break;

                    case MotionEvent.ACTION_UP:
                        if (fingerState != FINGER_DRAGGING) {
                            fingerState = FINGER_RELEASED;
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                            alertDialogBuilder.setTitle(R.string.det_launch_br);
                            alertDialogBuilder.setMessage(mUrl);
                            alertDialogBuilder.setPositiveButton(R.string.det_launch_ok,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Uri uri = Uri.parse(webView.getUrl());
                                            Intent i = new Intent(Intent.ACTION_VIEW, uri);
                                            startActivity(i);
                                        }
                                    });
                            alertDialogBuilder.setNegativeButton(R.string.det_launch_no,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    });

                            alertDialogBuilder.setCancelable(true);
                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } else if (fingerState == FINGER_DRAGGING) fingerState = FINGER_RELEASED;
                        else fingerState = FINGER_UNDEFINED;
                        break;

                    case MotionEvent.ACTION_MOVE:
                        if (fingerState == FINGER_TOUCHED || fingerState == FINGER_DRAGGING)
                            fingerState = FINGER_DRAGGING;
                        else fingerState = FINGER_UNDEFINED;
                        break;

                    default:
                        fingerState = FINGER_UNDEFINED;
                }

                return false;
            }
        });

        //actionbar
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(mName);
    }

    //電話番号処理
    @OnClick({ R.id.det_shop_number})
    void onClickButton(TextView textView) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(R.string.det_num_check);
        alertDialogBuilder.setMessage(mTel);
        alertDialogBuilder.setPositiveButton(R.string.det_num_ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Intent.ACTION_CALL,
                                Uri.parse("tel:" + mTel));
                        startActivity(intent);
                    }
                });
        alertDialogBuilder.setNegativeButton(R.string.det_num_no,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        alertDialogBuilder.setCancelable(true);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
