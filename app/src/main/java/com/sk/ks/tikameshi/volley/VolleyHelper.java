package com.sk.ks.tikameshi.volley;

import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import android.util.LruCache;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.sk.ks.tikameshi.BuildConfig;
import com.sk.ks.tikameshi.global.Global;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Volley
 * Singleton
 */
public class VolleyHelper {
    private static VolleyHelper mInstance = null;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    private static Map<String, String> scheme = new HashMap<String,String>();
    private static Map<String, String> authority = new HashMap<String,String>();

    private VolleyHelper(){
        final HurlStack stack = new HurlStack(null, Global.getInstance().getAllAllowsSocketFactory());
        mRequestQueue = Volley.newRequestQueue(Global.getInstance().getContext(), stack);
        mImageLoader = new ImageLoader(this.mRequestQueue, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(100);
            public void putBitmap(String url, Bitmap bitmap) {
                mCache.put(url, bitmap);
            }
            public Bitmap getBitmap(String url) {
                return mCache.get(url);
            }
        });
    }
    public static VolleyHelper getInstance(){
        if(mInstance == null){
            mInstance = new VolleyHelper();
        }
        return mInstance;
    }
    public RequestQueue getRequestQueue(){
        return this.mRequestQueue;
    }
    public ImageLoader getImageLoader(){
        return this.mImageLoader;
    }

    public interface exeQueueCallback {
        //  public void onSuccessCallback(int status, JSONObject response);
        public void onSuccessCallback(JSONObject response);
        public void onErrorCallback(VolleyError error);
    }


    public void exeQueueGET(String schema, String section,String path, Map<String, String> params, final exeQueueCallback instance) {
        // URL生成
        Uri.Builder builder = new Uri.Builder();
        builder.scheme(schema);
        builder.authority(section);
        builder.path(path);
        for (String key : params.keySet()) {
            builder.appendQueryParameter(key, params.get(key));
        }
        final String url = builder.build().toString();
        Log.v(this.getClass().getName(), "call:" + url);
        // キュー
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, (JSONObject)null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            instance.onSuccessCallback(response);


                        } catch (Exception e) {

                            Log.v(this.getClass().getName(), "onResponse:" + url);
                            Log.v(this.getClass().getName(), "Exception:" + e);
                            e.printStackTrace();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.v(this.getClass().getName(), "onError:" + error);
                        instance.onErrorCallback(error);
                    }
                }
        );
        // Volleyのタイムアウト時間設定
        request.setRetryPolicy(new ExRetryPolicy(request));
        this.mRequestQueue.add(request);
        this.mRequestQueue.start();

    }
}
