package com.sk.ks.tikameshi.adapter;

/**
 * Item
 * Getter / Setter
 */
public class RowItem {
    //店舗名称
    private String name;
    //路線名
    private String a_line;
    //駅
    private String a_station;
    //駅出口
    private String a_station_exit;
    //徒歩(分)
    private String a_walk;
    //店舗画像1
    private String shop_image1;
    //店舗画像2
    private String shop_image2;
    //住所
    private String address;
    //電話番号
    private String tel;
    //営業時間
    private String opentime;
    //緯度
    private String latitude;
    //経度
    private String longitude;
    //URL
    private String url;
    //備考
    private String a_note;


    public void setName(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getA_line() {
        return a_line;
    }

    public void setA_line(String a_line) {
        this.a_line = a_line;
    }

    public String getA_station() {
        return a_station;
    }

    public void setA_station(String a_station) {
        this.a_station = a_station;
    }

    public String getA_station_exit() {
        return a_station_exit;
    }

    public void setA_station_exit(String a_station_exit) {
        this.a_station_exit = a_station_exit;
    }

    public String getA_walk() {
        return a_walk;
    }

    public void setA_walk(String a_walk) {
        this.a_walk = a_walk;
    }

    public String getShop_image1() {
        return shop_image1;
    }

    public void setShop_image1(String shop_image1) {
        this.shop_image1 = shop_image1;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getOpentime() {
        return opentime;
    }

    public void setOpentime(String opentime) {
        this.opentime = opentime;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getA_note() {
        return a_note;
    }

    public void setA_note(String a_note) {
        this.a_note = a_note;
    }

    public String getShop_image2() {
        return shop_image2;
    }

    public void setShop_image2(String shop_image2) {
        this.shop_image2 = shop_image2;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
