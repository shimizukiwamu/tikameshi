package com.sk.ks.tikameshi.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.android.volley.toolbox.ImageLoader;
import com.sk.ks.tikameshi.R;
import com.sk.ks.tikameshi.activity.DetailActivity;
import com.sk.ks.tikameshi.activity.ResultsActivity;
import com.sk.ks.tikameshi.volley.CustomNetWorkImageView;
import com.sk.ks.tikameshi.volley.VolleyHelper;
import java.util.List;

/**
 *  ArrayAdapter　
 *  検索結果画面のListVie用
 */
public class RowAdapter extends ArrayAdapter<RowItem> {
    private LayoutInflater mFactory;
    private int mItemLayoutResource;
    private ImageLoader ilImage;

    //コンストラクタ
    public RowAdapter(Context context, int resource, List<RowItem> objects) {
        super(context, resource, objects);
        mFactory = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mItemLayoutResource = resource;
    }

    //listviewの各rowの処理
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            convertView = mFactory.inflate(mItemLayoutResource, null);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.row_nameView);
            holder.access = (TextView) convertView.findViewById(R.id.row_accessView);
            holder.icon = (CustomNetWorkImageView) convertView.findViewById(R.id.row_iconView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final RowItem item = getItem(position);
        holder.name.setText(item.getName());
        holder.access.setText(createAccess(item));
        ilImage = VolleyHelper.getInstance().getImageLoader();
        holder.icon.setImageUrl(item.getShop_image1(), ilImage);
        holder.icon.setDefaultImageResId(R.mipmap.no_image);
        holder.icon.setErrorImageResId(R.mipmap.no_image);

        //listvieのClickListener
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ResultsActivity activity = (ResultsActivity) getContext();
                Intent intent = new Intent(activity, DetailActivity.class);
                intent.putExtra("shop_name", item.getName());
                intent.putExtra("shop_address",item.getAddress());
                intent.putExtra("shop_number",item.getTel());
                intent.putExtra("shop_opentime",item.getOpentime());
                intent.putExtra("shop_image",item.getShop_image1());
                intent.putExtra("shop_url",item.getUrl());
                activity.startActivity(intent);
            }

        });
        return convertView;
    }
    //アクセスに表示する文字列作成メソッド
    public String createAccess(RowItem item){
        String access ="";
        access = access+item.getA_line()+" "+item.getA_station()+" "+item.getA_station_exit();
        try {
            int walk = Integer.parseInt(item.getA_walk());
            access = access+"徒歩"+item.getA_walk()+"分";
        } catch (NumberFormatException e) {
            access = access+item.getA_walk();
        }
        access = access.replaceAll("([\\{\\}])", "");

        return access;
    }
    //viewholder
    class ViewHolder {
        TextView name;
        TextView access;
        CustomNetWorkImageView icon;
    }
}