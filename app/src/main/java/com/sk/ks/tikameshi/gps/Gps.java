package com.sk.ks.tikameshi.gps;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

import com.sk.ks.tikameshi.global.Global;

import java.lang.reflect.Array;
import java.util.List;

/**
 * gps取得クラス
 */
public class Gps  {
    private Context context;

    //コンストラクタ
    public Gps (){
        this.context = Global.getInstance().getContext();
    }

    public String[] getGps() {
        String latitude ="";
        String longtitude ="";
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        // ネットワーク取得
        if(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            Location locationNetwork = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (locationNetwork != null) {
               Double lat =locationNetwork.getLatitude();
                Double longt  = locationNetwork.getLongitude();
                latitude = String.valueOf(lat);
                longtitude = String.valueOf(longt);
            }
        }

        // GPS取得
        if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if(locationGPS != null){
                Double lat =locationGPS.getLatitude();
                Double longt  = locationGPS.getLongitude();
                latitude = String.valueOf(lat);
                longtitude = String.valueOf(longt);
            }
        }
        String[] gps = {latitude,longtitude};
        Log.v("provider","provider"+gps);
        return gps;

    }
}
