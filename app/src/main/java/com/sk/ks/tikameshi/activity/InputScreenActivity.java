package com.sk.ks.tikameshi.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;


import com.sk.ks.tikameshi.R;
import com.sk.ks.tikameshi.adapter.DrawerAdapter;
import com.sk.ks.tikameshi.gnavi.Gnavi;
import com.sk.ks.tikameshi.gps.Gps;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Activity
 * 検索条件入力画面
 */
public class InputScreenActivity extends AppCompatActivity  {
    //drawer
    DrawerLayout dLayout;
    private ListView dListView;
    private ArrayList<String> rangeList;
    private DrawerAdapter draweradapter;
    //butter knife
    @Bind(R.id.search_button) Button search_Button;
    @Bind(R.id.clear_button) Button clear_Button;
    @Bind(R.id.search_range_str) TextView rangeTextView;
    private EditText keyText;
    private int mRange = 1; //500m

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_input_screen);
        ButterKnife.bind(this);


        keyText = (EditText) findViewById(R.id.search_keyword);
        keyText.setText("");

        dLayout = (DrawerLayout) findViewById(R.id.NewsDrawerLayout);
        dLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {


            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
        dListView = (ListView) findViewById(R.id.drawer_range_listview);
        String[] rangeArray =  getResources().getStringArray(R.array.rang_list);
        rangeList =  new ArrayList<String>();
        Collections.addAll(rangeList,rangeArray);
        draweradapter = new DrawerAdapter(this,0,rangeList);
        dListView.setAdapter(draweradapter);
        dListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position, long id) {
                mRange = position;
                rangeTextView.setText(rangeList.get(mRange));
                dLayout.closeDrawers();

            }
        });
        rangeTextView.setText(rangeList.get(mRange));

    }
    @OnClick({ R.id.search_range_str})
    void onClickRange (TextView rangeView){
        dLayout.openDrawer(Gravity.LEFT);
    }

    @OnClick({ R.id.search_button})
    void onClickButton(Button button) {
        Intent intent = new Intent(this, ResultsActivity.class);
        String keyword = String.valueOf(keyText.getText());
        String range = Integer.toString(mRange);
        intent.putExtra("range", range);
        intent.putExtra("keyword", keyword);
        startActivity(intent);

    }
    @OnClick({R.id.clear_button})
    void onClearButon(Button clearButton){
        rangeTextView.setText(rangeList.get(1));
        keyText.setText("");
    }
}