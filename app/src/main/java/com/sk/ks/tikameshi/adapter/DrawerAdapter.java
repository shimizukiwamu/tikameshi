package com.sk.ks.tikameshi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sk.ks.tikameshi.R;

import java.util.ArrayList;

/**
 * ArrayAdapter
 * Drawer用
 */
public class DrawerAdapter extends ArrayAdapter<String> {

    private LayoutInflater layoutInflater;


    private static class ViewHolder {
        TextView rangeTextView;

        public ViewHolder(View view) {
            this.rangeTextView = (TextView) view.findViewById(R.id.drawer_range);
        }
    }


    public DrawerAdapter(Context context, int resource,ArrayList<String>rangeList) {
        super(context, resource,rangeList);
        this.layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        ViewHolder holder;
        if(convertView == null){
            convertView = layoutInflater.inflate(R.layout.drawer_row,null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final String range =  (String)getItem(position);

        holder.rangeTextView.setText(range);
        return convertView;
    }
}